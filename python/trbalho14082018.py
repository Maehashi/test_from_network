# -*- coding: latin-1 -*-


#Logiaca para codificar e ler uma paçavra

class Camada(object):
    def __init__(self,name,camada_sup=None,camada_inf=None):
        self.name = name
        self.camada_sup = camada_sup
        self.camada_inf = camada_inf

    def env(self,dados):
        self.camada_inf.env(dados)
    
    def rec(self,dados):
        self.camada_sup.rec(dados)

class CamadaSuperior(Camada):

    def rec(self,dados):
        print(self.name,self.__class__.__name__,dados)

class CamadaInferior(Camada):

    def __init__(self,*args,**kwargs):
        super(CamadaInferior,self).__init__(*args,**kwargs)
        self.buffer = []

    def env(self,dados):
        for c in dados:
            self.camada_inf.rec(c)
        self.camada_inf.rec(None)

    def rec(self,dados):
        if dados is None:
            self.camada_sup.rec(''.join(self.buffer))
            self.buffer = []
        self.buffer.append(dados)



class CamadaInversora(Camada):

    def env(self,dados):
        self.camada_inf.env(dados[::-1])
    
    def rec(self,dados):
        self.camada_sup.rec(dados[::-1])


#Estanciando
ca3 = CamadaSuperior('A')
ca2 = CamadaInversora('A')
ca1 = CamadaInferior('A')

cb3 = CamadaSuperior('B')
cb2 = CamadaInversora('B')
cb1 = CamadaInferior('B')

#Definir o caminho das classes 
ca3.camada_inf = ca2
ca2.camada_sup = ca3
ca2.camada_inf = ca1
ca1.camada_sup = ca2

cb3.camada_inf = cb2
cb2.camada_sup = cb3
cb2.camada_inf = cb1
cb1.camada_sup = cb2

ca1.camada_inf = cb1
cb1.camada_inf = ca1

#Definindo dados
ca3.env('teste')
cb3.env('outro teste')