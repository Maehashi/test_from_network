# Cliente UDP
# nome: Johan Rafhael Maehashi
# Data:30/ 10/ 2018


import socket
 
UDP_IP = "127.0.0.1"
UDP_PORT = 5005
MESSAGE = str("Hello, World!")

print ("UDP target IP:", UDP_IP)
print ("UDP target port:", UDP_PORT)
print ("message:", MESSAGE)
   
sock = (socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM)) # UDP
sock.sendto(MESSAGE.encode('utf-8'),(UDP_IP, UDP_PORT))
#sock.bind((UDP_IP, UDP_PORT))
while True:
    
    data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
    if data != None:
        print ("received message:", data)
        break