import rsa
import hashlib

(kpub, kpriv)= rsa.newkeys(1024)

entrada = input('Digite a menssagem a ser criptografada\n')

menssagem = entrada.encode('utf8')

mensagemHash = hashlib.sha256(menssagem)

criptado = rsa.encrypt(menssagem, kpub)

print ("\nmenssagem criptografada\n")
print (criptado)

print ("\nmenssagem hash\n")
print (mensagemHash.hexdigest())

decriptado = rsa.decrypt(criptado, kpriv)

mensagemHash2 = hashlib.sha256(decriptado)

print (mensagemHash2.hexdigest())

print ("\nmenssagem decriptada\n")
print (decriptado)

if (mensagemHash.hexdigest() == mensagemHash2.hexdigest()):
    print ("mensagem esta integra")
else:
    print ("mensagem foi modificada")