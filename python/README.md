
## Estrutura de um Pacote IPv4 – Redes de Computadores

![Estrutura](http://www.bosontreinamentos.com.br/wp-content/uploads/2016/06/pacote-IP-estrutura.png)

img src="img src="http://cdn.osxdaily.com/wp-content/uploads/2013/07/dancing-banana.gif" alt="Banana" />
" alt="Banana" />


* Versão: Indica a versão do protocolo IP que está sendo usado. Para o protocolo IPv4, o valor 4 é utilizado neste campo.

* Tamanho do cabeçalho (IHL, Internet Header Length): Indica o comprimento do cabeçalho do pacote em número de palavras de 32 bits (bits existentes no cabeçalho / 32). Como o campo de opções pode ter tamanho variável, é importante registrar aqui o tamanho do cabeçalho, para que seja possível definir onde ele termina, e onde se inicia o campo de dados. Para um pacote que não usa nenhuma opção, o valor do IHL será de 160 / 32 = 5, sendo que 160 é o somatório de bits que compõem o cabeçalho do pacote.

* DSCP: Differentiated Services Code Point. Esse campo originalmente era definido como ToS (Type of Service / Tipo de Serviço). É utilizado por tecnologias que necessitem de streaming de dados em tempo-real, como por exemplo VoIP (Voz sobre IP).

* ECN: Explicit Congestion Notification, este campo permite uma notificação fim-a-fim de congestionamento de rede sem descartar pacotes. É um campo de uso opcional.
Tamanho Total: Define o tamanho total do pacote em bytes, incluindo o cabeçalho e o campo de dados.
Identificação: Identifica o Datagrama (pacote) IP por meio de um número de identificação sequencial.
Flags: Usado para controlar a fragmentação dos pacotes. Possui 3 bits, com o seguinte significado (do maior para o menor):
    * Bit 0: O primeiro bit é sempre 0 (reservado).
    * Bit 1: DF (Don´t Fragment), quando ativado indica que o datagrama não pode ser fragmentado. Se a fragmentação for requerida na rede e este bit estiver ativado, o pacote será descartado.
     * Bit 2: MF (More Fragments):Indica o último fragmento: quando o bit for 1, há mais fragmentos depois dele; quando for 0, ele é o último fragmento do conjunto do pacote.

* Offset do Fragmento: Determina a ordem dos fragmentos. Especifica o deslocamento relativo de um fragmento em particular em relação ao início do pacote IP original, sem fragmentação. O primeiro fragmento possui offset igual a zero.
É medido em blocos de oito bytes.

* Tempo de Vida (TTL): Tempo de vida máximo do Datagrama. Impede que um pacote se perca em uma rede (Internet) e entre em loop infinito entre os roteadores. Atualmente seu valor corresponde a saltos de roteadores (hops). Toda vez que um pacote passa por um roteador, o valor desse campo é decrementado (subtrai-se 1). Quando um roteador recebe um pacote cujo TTL é igual a 0, o pacote é descartado, evitando sua permanência infinita na rede.
* Protocolo: Indica o protocolo (presente no campo de dados) que pediu o envio do datagrama, através de um código numérico. Os códigos são definidos pela RFC 790.
* Checksum do Cabeçalho: Usado para verificação de erros no cabeçalho do datagrama. Quando um pacote chega em um roteador, o roteador calcula o checksum do cabeçalho e compara o valor obtido com o valor armazenado nesse campo. Se os valores não baterem, significa que houve erro durante a transissão dos dados, e o pacote é descartado.
Toda vez que um pacote passa por um roteador, um novo checksum deve ser calculado e armazenado neste campo, pois os roteadores alteram o conteúdo do cabeçalho IP ao decrementar o campo de TTL.

* Endereço IP de Origem: Endereço IP do remetente do pacote. Esse endereço pode ser alterado por um roteador se for utilizado um serviço de tradução de endereços, como o NAT.

* Endereço IP de Destino: Endereço IP do destinatário do pacote. Esse endereço também pode ser alterado por um roteador se for utilizado um serviço de tradução de endereços, como o NAT.

* Opções + Pad: Campo opcional. Usado em situações de teste e verificação de erros na rede. As duas funções mais importantes desse campo são traçar a rota de rede que está sendo usada da origem até o destino (traceroute) e marcar o horário com que o datagrama passa por cada roteador da origem até o destino (timestamp)
Dados: São os dados que o datagrama está carregando, com o limite de 64 kB. Os dados não fazem parte do cálculo do checksum do cabeçalho. Os protocolos mais comuns encontrados no campo de dados são o TCP, UDP, ICMP, IGMP e OSPF, entre outros.

