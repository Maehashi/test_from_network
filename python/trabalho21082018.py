import math
from itertools import chain

#Logica de verificação de de linguagem binaria

#Execudar o log
def parity_bits_quantity(word_size):
    return int(math.log(word_size,2))+1

#Classificador de checagem

def parity_indexes(parity_bit_index, word_size):
    #Lupi que faz a verificação de acondo com o indice da paridade
    for i in range(parity_bit_index,word_size+1,parity_bit_index*2):
        yield range(i,i+parity_bit_index)

#Verificador dos bits
def check_parity(index,word):
    bits_to_check = chain(*parity_indexes(index,len(word)))

#
def check(word):
    size = len(word)
    parities_quantity = parity_bits_quantity(size)
    for p in range(parities_quantity):
        index = 2 ** p
        check_parity(index,word)

check('1110111')